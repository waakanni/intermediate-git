# Bergheim

### Don Robert

Alte Eppelheimer Str. 11, 69115 Heidelberg

http://www.don-robert.com/

#### Pro

- good tapas
- plenty of vegetarian options

#### Con

- can be difficult to find
